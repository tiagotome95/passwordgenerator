'''
 Copyright (C) 2021  Tiago Simões Tomé

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 PasswordGenerator is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''
#!/usr/bin/python3
# -*- coding: utf-8 -*-
from hashlib import pbkdf2_hmac

lower_case_letters = list("abcdefghijklmnopqrstuvwxyz")
upper_case_letters = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
numbers = list("1234567890")
special_characters = list("#!$€%^&*()_-+=/{}[]?<>.,;:")
passwort_characters = lower_case_letters + upper_case_letters + numbers + special_characters
salt = "pepper"

def getSalt():
    print(salt)
    return salt

def convert_bytes_to_passwort(hashed_bytes, length):
    number = int.from_bytes(hashed_bytes, byteorder = "big")
    passwort = ""
    while number > 0 and len(passwort) < length:
        passwort = passwort + passwort_characters[number %
                                                  len(passwort_characters)]
        number = number // len(passwort_characters)
    return passwort

def makePassword(saltIn, domain, masterPassword):
    hash_string = str(domain) + str(masterPassword)
    salt = str(saltIn)
    hashed_bytes = pbkdf2_hmac("sha512", hash_string.encode("utf-8"), salt.encode("utf-8"), 4096)
    passwordOutput = str(convert_bytes_to_passwort(hashed_bytes, 10))
    return passwordOutput


def speak(text):
    print(text)
    return text
