/*
 * Copyright (C) 2021  Tiago Simões Tomé
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * PasswordGenerator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import QtQml 2.2
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import io.thp.pyotherside 1.3

Rectangle {
    id: root
    property string headerSuffix: i18n.tr("Settings")
        color: theme.palette.normal.background
    anchors {
        top: header.bottom
        left: parent.left
        right: parent.right
        bottom: parent.bottom
    }

    ColumnLayout {
        id: contentColumn0

        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
        }
        spacing: units.gu(1)

        ListItem {
            id: sectionLabel

            Layout.preferredHeight: units.gu(4)
            divider.visible: true
            divider.height: units.gu(0.2)
            highlightColor: "transparent"

            Layout.fillWidth: true

            ListItemLayout {
                padding.top: 0
                padding.bottom: units.gu(1)
                anchors.centerIn: parent
                title.text: i18n.tr("Default Profile Settings")
                title.font.weight: Font.DemiBold
            }
        }

        // Default Salt
        ListItem {
            id: saltListitem
            Layout.fillWidth: true

            ListItemLayout {
                anchors.centerIn: parent
                title.text: i18n.tr("Default Salt")

                TextField {
                    id: saltField
                    placeholderText: i18n.tr("Salt")
                    //text: "Pepper"
                    SlotsLayout.position: SlotsLayout.Trailing
                }
            }
        }

        // Default SpecialCharacters
        ListItem {
            id: specialCharactersListitem
            Layout.fillWidth: true

            ListItemLayout {
                anchors.centerIn: parent
                title.text: i18n.tr("Default Special Characters")

                TextField {
                    id: specialCharactersField
                    placeholderText: i18n.tr("?!#")
                    //text: "Pepper"
                    SlotsLayout.position: SlotsLayout.Trailing
                }
            }
        }

        // Default PasswordLenght
        ListItem {
            id: passwordLenghtListitem
            Layout.fillWidth: true
            divider.visible: true
            divider.height: units.gu(0.3)

            ListItemLayout {
                id: passwordLenghtListitemLayout
                anchors.centerIn: parent
                title.text: i18n.tr("Password lenght")

                Slider {
                    id: passwordLenghtSlider
                    anchors.centerIn: parent.left
                    width: units.gu(20)
                    value: 10
                    minimumValue: 4
                    maximumValue: 64
                    stepSize: 1
                    live: true
                }
                TextField {
                    id: passwordLenghtSpinbox
                    anchors.verticalCenter: parent.right
                    width: units.gu(4.5)
                    text: passwordLenghtSlider.value.toFixed(0)
                    validator: IntValidator{bottom: 4; top: 64;}
                    hasClearButton: false
                }
            }
        }

        // Domain Profiles
        ListItem {
            id: domainProfilesListitem
            Layout.fillWidth: true

            ListItemLayout {
                anchors.centerIn: parent
                title.text: i18n.tr("Domain Profiles")


            }
        }

    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));

            importModule('main', function() {});
            console.log('module imported');

            python.call('main.getSalt', [], function(returnValue) {
                saltField.text = returnValue
                console.log('main.getSalt returned ' + returnValue);
            })
            python.call('main.makePassword', [], function(returnValue) {
                passwordOutput.text = returnValue
                console.log('main.makePassword');
            })

        }

        onError: {
            console.log('python error: ' + traceback);
        }

    }

}
