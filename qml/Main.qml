/*
 * Copyright (C) 2021  Tiago Simões Tomé
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * PasswordGenerator is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3

import "panels"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'passwordgenerator.tiagotome95'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent
        header: PageHeader {
            id: header

            StyleHints {
                foregroundColor: UbuntuColors.orange
                //backgroundColor: UbuntuColors.slate
                dividerColor: UbuntuColors.orange
            }
            leadingActionBar.actions: [
                Action{
                    iconName: "back"
                    visible: stack.depth>1
                    onTriggered: stack.pop()
                }
            ]
            trailingActionBar.actions: [
                Action{
                    iconName: "settings"
                    onTriggered: {
                        while (stack.depth>1 && stack.currentItem!==settingsPanel) stack.pop()
                        if (stack.currentItem!==settingsPanel) {
                            stack.push(settingsPanel)
                        }
                    }
                    visible: stack.currentItem === calcPanel
                },
                Action{
                    iconName: "add"
                    onTriggered: {
                        while (stack.depth>1 && stack.currentItem!==domainProfilePanel) stack.pop()
                        if (stack.currentItem!==domainProfilePanel) {
                            stack.push(domainProfilePanel)
                        }
                    }
                    visible: stack.currentItem === calcPanel
                },
                // when NewDomainProfilePanel opens
                Action{
                    iconName: "ok"
                    onTriggered: {
                        stack.pop()
                    }
                    visible: stack.currentItem === domainProfilePanel
                }
            ]
        }

        StackView{
            id: stack
            anchors.fill: parent
            onCurrentItemChanged: header.title = currentItem.headerSuffix
        }

        CalcPanel{
            id: calcPanel
            Component.onCompleted: stack.push(calcPanel)
        }

        SettingsPanel{
            id: settingsPanel
            visible: false
        }

        NewDomainProfilePanel{
            id: domainProfilePanel
            visible: false
        }

    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));

            importModule('main', function() {});
            console.log('module imported');

            python.call('main.getSalt', [], function(returnValue) {
                saltField.text = returnValue
                console.log('main.getSalt returned ' + returnValue);
            })
            python.call('main.makePassword', [], function(returnValue) {
                passwordOutput.text = returnValue
                console.log('main.makePassword');
            })

        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }

}
